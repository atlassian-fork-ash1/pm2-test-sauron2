data "template_file" "pollinator_check_src" {
  template = "${file("${path.module}/pm2testcheck_sauron.ts")}"
}

resource "pollinator_check" "pollinator_check_pm2testcheck_sauron" {
  name = "PM2TestCheck-Sauron-Integration_Test-${var.pollinator_envtype}"
  functype = "noncore"

  synthetic_check = {
    script_body = "${data.template_file.pollinator_check_src.rendered}"
    # TODO This is dynamic, you can use any key/value pairs needed
    arguments = {
      getUrlUnderTest = "/REPLACE_URL"
    }
  }

  credentials = {
    token = "${var.token}"
  }

  access_control = {
    group = "${var.ad-group}"
  }

  notifications = [
    {
      type = "slack"
      target = "${var.slack-channel}"
      trigger_levels = [
        "OK",
        "CRITICAL"
      ]
    }
  ]
}
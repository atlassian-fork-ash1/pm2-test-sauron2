
variable "pollinator_team" {
  default = "jsre"
}

variable "ad-group" {
  default = "jira-polli-access-dl-jira-polli-access"
}

variable "slack-channel" {
  default = "test-channel"
}

variable "pollinator_envtype" {
  default = "dev"
}

variable "token" {
  default = "dummy-pollinator-token"
}

variable "username" { default = "username" }
variable "password" { default = "password" }